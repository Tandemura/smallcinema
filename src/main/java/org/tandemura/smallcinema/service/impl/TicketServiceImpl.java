package org.tandemura.smallcinema.service.impl;

import org.tandemura.smallcinema.dao.TicketDao;
import org.tandemura.smallcinema.dao.impl.TicketDaoImpl;
import org.tandemura.smallcinema.model.Ticket;
import org.tandemura.smallcinema.service.TicketService;

import java.util.List;

import static org.tandemura.smallcinema.service.util.ServiceValidation.checkNotFound;

/**
 * Provides implementation of all {@code TicketService} interface methods.
 */
public class TicketServiceImpl implements TicketService {

    private TicketDao ticketDao;
    private static volatile TicketService service;

    private TicketServiceImpl() {
        ticketDao = TicketDaoImpl.getTicketDao();
    }

    public static TicketService getTicketService() {
        if (service == null) {
            synchronized (TicketServiceImpl.class) {
                if (service == null) {
                    service = new TicketServiceImpl();
                }
            }
        }
        return service;
    }

    @Override
    public List<Ticket> getAllTicketsByEventId(Integer id) {
        return ticketDao.getAllByEventId(id);
    }

    @Override
    public List<Ticket> getAllTicketsByUserId(Integer id) {
        //TODO date after today
        return ticketDao.getAllByUserId(id);
    }

    @Override
    public void saveAll(List<Ticket> tickets) {
        checkNotFound(tickets, "tickets must not be null");
        ticketDao.saveAll(tickets);
    }
}
