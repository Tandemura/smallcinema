package org.tandemura.smallcinema.service.impl;

import org.tandemura.smallcinema.dao.BaseDao;
import org.tandemura.smallcinema.dao.impl.DaoFactory;
import org.tandemura.smallcinema.model.Event;
import org.tandemura.smallcinema.service.EventService;

import java.time.LocalDate;
import java.util.List;

import static org.tandemura.smallcinema.service.util.ServiceValidation.checkNotFound;
import static org.tandemura.smallcinema.service.util.ServiceValidation.checkNotFoundWithId;

public class EventServiceImpl implements EventService {
    private BaseDao eventDao;
    private static volatile EventService service;

    private EventServiceImpl() {
        eventDao = DaoFactory.getDao(DaoFactory.DaoType.EVENT);
    }

    public static EventService getEventService() {
        if (service == null) {
            synchronized (EventServiceImpl.class) {
                if (service == null) {
                    service = new EventServiceImpl();
                }
            }
        }
        return service;
    }

    @Override
    public List<Event> getEvents(LocalDate date) {
        return eventDao.getAllByDate(date);
    }

    @Override
    public Event getEvent(Integer id) {
        return checkNotFoundWithId(eventDao.getById(id), id);
    }

    @Override
    public void delete(int id) {
        checkNotFoundWithId(eventDao.delete(id), id);
    }

    @Override
    public void save(Event event) {
        checkNotFound(event, "event must not be null");
        checkNotFound(eventDao.save(event), "user must not be null");
    }
}

