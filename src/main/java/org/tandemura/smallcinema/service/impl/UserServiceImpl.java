package org.tandemura.smallcinema.service.impl;


import org.tandemura.smallcinema.dao.BaseDao;
import org.tandemura.smallcinema.dao.impl.DaoFactory;
import org.tandemura.smallcinema.model.User;
import org.tandemura.smallcinema.service.UserService;
import org.tandemura.smallcinema.service.util.UserCache;

import java.util.List;

import static org.tandemura.smallcinema.service.util.ServiceValidation.checkNotFound;
import static org.tandemura.smallcinema.service.util.ServiceValidation.checkNotFoundWithId;

/**
 * Provides implementation of all {@code UserService} interface methods.
 * All modification operations invalidate cached collection of users.
 */
public class UserServiceImpl implements UserService {

    private final String userNotNullMessage = "user must not be null";
    private final String emailNotNullMessage = "email must not be null";
    private final BaseDao userDao;
    private static volatile UserService service;

    private UserServiceImpl() {
        userDao = DaoFactory.getDao(DaoFactory.DaoType.USER);
    }

    public static UserService getUserService() {
        if(service ==null) {
            synchronized (UserServiceImpl.class) {
                if(service ==null) {
                    service = new UserServiceImpl();
                }
            }
        }
        return service;
    }

    @Override
    public void create(User user) {
        checkNotFound(user, userNotNullMessage);
        user.setPassword(user.getPassword());
        checkNotFound(userDao.save(user), userNotNullMessage);
        UserCache.invalidate();
    }

    @Override
    public void delete(int id) {
        checkNotFoundWithId(userDao.delete(id), id);
        UserCache.invalidate();
    }

    @Override
    public User get(int id) {
        return checkNotFoundWithId(userDao.getById(id), id);
    }

    @Override
    public User getByEmail(String email) {
        checkNotFound(email, emailNotNullMessage);
        return checkNotFound(userDao.getByValue(email), "email=" + email);
    }

    @Override
    public void update(User user) {
        checkNotFound(user, userNotNullMessage);
        if(!user.getPassword().isEmpty()) {
            user.setPassword(user.getPassword());
        }
        checkNotFoundWithId(userDao.save(user), user.getId());
        UserCache.invalidate();
    }

    @Override
    public List<User> getAll() {
        return userDao.getAll();
    }
}

