package org.tandemura.smallcinema.service.impl;

import org.tandemura.smallcinema.dao.BaseDao;
import org.tandemura.smallcinema.dao.impl.DaoFactory;
import org.tandemura.smallcinema.model.Movie;
import org.tandemura.smallcinema.service.MovieService;
import org.tandemura.smallcinema.service.util.MovieCache;

import java.util.List;

import static org.tandemura.smallcinema.service.util.ServiceValidation.checkNotFound;
import static org.tandemura.smallcinema.service.util.ServiceValidation.checkNotFoundWithId;

/**
 * Provides implementation of all {@code MovieService} interface methods.
 * All modification operations invalidate cached collection of movies.
 */
public class MovieServiceImpl implements MovieService {

    private String movieNotNullMessage = "movie must not be null";
    private BaseDao movieDao;
    private static volatile MovieService service;

    private MovieServiceImpl() {
        movieDao = DaoFactory.getDao(DaoFactory.DaoType.MOVIE);
    }

    public static MovieService getMovieService() {
        if(service ==null) {
            synchronized (MovieServiceImpl.class) {
                if(service ==null) {
                    service = new MovieServiceImpl();
                }
            }
        }
        return service;
    }

    @Override
    public void create(Movie movie) {
        checkNotFound(movie, movieNotNullMessage);
        checkNotFound(movieDao.save(movie), movieNotNullMessage);
        MovieCache.invalidate();
    }

    @Override
    public void delete(int id) {
        checkNotFoundWithId(movieDao.delete(id), id);
        MovieCache.invalidate();
    }

    @Override
    public Movie get(int id) {
        return checkNotFoundWithId(movieDao.getById(id), id);
    }

    @Override
    public void update(Movie movie) {
        checkNotFound(movie, movieNotNullMessage);
        checkNotFoundWithId(movieDao.save(movie), movie.getId());
        MovieCache.invalidate();
    }

    @Override
    public List<Movie> getAllMovies() {
        return movieDao.getAll();
    }

}
