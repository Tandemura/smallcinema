package org.tandemura.smallcinema.service.util;

import org.tandemura.smallcinema.dao.impl.DaoFactory;
import org.tandemura.smallcinema.model.Entity;
import org.tandemura.smallcinema.model.Movie;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemura.smallcinema.model.Movie.NameComparator;

/**
 * Provides local storage of list of movies.
 * Every modification operation (create, update, delete) clears the stored list.
 */

public class MovieCache {
    private static volatile List<Movie> movies = new ArrayList<>();

    public static List<Movie> getMovies() {
        if (movies.isEmpty()) {
            synchronized (UserCache.class) {
                if (movies.isEmpty()) {
                    movies = DaoFactory.getDao(DaoFactory.DaoType.MOVIE).getAll();
                }
            }
        }
        return movies;
    }

    public static List<Movie> getActiveMovies() {
        return getMovies().stream().filter(Movie::isActive).collect(Collectors.toList());
    }

    public static List<Movie> getMoviesSortedByName() {
        return getMovies().stream().sorted(NameComparator).collect(Collectors.toList());
    }
    public static synchronized void invalidate() {
        movies.clear();
    }
}
