package org.tandemura.smallcinema.service.util;

import org.tandemura.smallcinema.dao.impl.DaoFactory;
import org.tandemura.smallcinema.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides local storage of list of users.
 * Every modification operation (create, update, delete) clears the stored list.
 */
public class UserCache {

    private static volatile List<User> users = new ArrayList<>();

    public static List<User> getUsers() {
        if (users.isEmpty()) {
            synchronized (UserCache.class) {
                if (users.isEmpty()) {
                    users = DaoFactory.getDao(DaoFactory.DaoType.USER).getAll();
                }
            }
        }
        return users;
    }

    public static synchronized void invalidate() {
        users.clear();
    }
}