package org.tandemura.smallcinema.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.dao.exception.DBException;
import org.tandemura.smallcinema.model.Movie;
import org.tandemura.smallcinema.service.MovieService;
import org.tandemura.smallcinema.service.impl.MovieServiceImpl;
import org.tandemura.smallcinema.service.exception.NotFoundEntityException;
import org.tandemura.smallcinema.service.util.MovieCache;
import org.tandemura.smallcinema.service.util.Paginator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The {@code MovieManagerServlet} class provides one of main functional for the Admins (CRUD).
 * - View the list of movies.
 * - Create new movie.
 * - Edit created movie data.
 * - Delete the movies from database.
 * Redirects to error page if some exception comes from DAO or service layer.
 */
public class MovieManagerServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(MovieManagerServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        MovieService service = MovieServiceImpl.getMovieService();

        String action = req.getParameter("action");
        String mid = req.getParameter("mid");

        try {
            switch (action == null ? "all" : action) {
                case "edit":
                case "create":
                    final Movie movie = "create".equals(action) ?
                            new Movie() : service.get(Integer.parseInt(mid));
                    LOGGER.debug("Movie obtained");
                    req.setAttribute("movie", movie);
                    req.getRequestDispatcher("jsp/movieForm.jsp").forward(req, resp);
                    break;
                case "delete":
                    service.delete(Integer.parseInt(mid));
                    LOGGER.info("Movie deleted: ", mid);
                    resp.sendRedirect("movies");
                    break;
                case "all":
                    Paginator<Movie> paginator = new Paginator<>();
                    List<Movie> movies = MovieCache.getMovies();
                    String sPage = req.getParameter("page");
                    int page = sPage != null ? Integer.parseInt(sPage) : 1;
                    int pagesCount = paginator.getPageCount(movies);
                    page = page > pagesCount ? pagesCount : Math.max(page, 1);
                    req.setAttribute("movies", paginator.getPage(movies, page));
                    req.setAttribute("page", page);
                    req.setAttribute("pagesCount", paginator.getPageCount(movies));
                    req.getRequestDispatcher("jsp/movies.jsp").forward(req, resp);
                    break;
                default:
                    req.getRequestDispatcher("jsp/movies.jsp").forward(req, resp);
                    break;
            }
        } catch (NumberFormatException | NotFoundEntityException e) {
            LOGGER.warn(e.getMessage());
            resp.setStatus(404);
            req.getRequestDispatcher("jsp/errorpages/error404.jsp").forward(req, resp);
        } catch (DBException e) {
            LOGGER.error(e);
            resp.setStatus(500);
            req.getRequestDispatcher("jsp/errorpages/error500.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String name = req.getParameter("name");
        String genre = req.getParameter("genre");
        String duration = req.getParameter("duration");
        String year = req.getParameter("year");
        String active = req.getParameter("status");
        String description = req.getParameter("description");
        String poster = req.getParameter("poster");

        MovieService service = MovieServiceImpl.getMovieService();

        try {
            Movie movie = new Movie(name, genre, Integer.parseInt(duration), Integer.parseInt(year), Boolean.valueOf(active), description, poster);

            if (req.getParameter("mid").isEmpty()) {
                service.create(movie);
            } else {
                movie.setId(Integer.parseInt(req.getParameter("mid")));
                service.update(movie);
            }
            resp.sendRedirect("movies");
        } catch (DBException e) {
            LOGGER.error(e);
            resp.setStatus(500);
            req.getRequestDispatcher("jsp/errorpages/error500.jsp").forward(req, resp);
        }
    }
}
