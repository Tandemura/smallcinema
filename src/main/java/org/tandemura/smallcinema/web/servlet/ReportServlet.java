package org.tandemura.smallcinema.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.model.Ticket;
import org.tandemura.smallcinema.model.User;
import org.tandemura.smallcinema.service.impl.TicketServiceImpl;

import org.tandemura.smallcinema.service.util.UserCache;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class ReportServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(ReportServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/reportEventUser.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        StringBuilder sb = new StringBuilder();
        String s = "";
        String doReport = req.getParameter("rep");
        if (doReport != null) {
            List<User> users = UserCache.getUsers();
            for (User u : users) {
                int countOfTickets = 0;
                List<Ticket> tickets;
                tickets = TicketServiceImpl.getTicketService().getAllTicketsByUserId(u.getId());
                countOfTickets = tickets.size();
                sb.append(u.getName());
                sb.append(";");
                sb.append(countOfTickets);
                sb.append("\n");
            }
        }
        s = sb.toString();
//        try (FileOutputStream fos = new FileOutputStream(req.getContextPath() + "//META-INF//reports//report.csv")) {
        try (FileOutputStream fos = new FileOutputStream("d://report.csv")) {
//        try (FileOutputStream fos = new FileOutputStream(getServletContext().getRealPath(".") + "reports.report.csv")) {
            byte[] buffer = s.getBytes();
            fos.write(buffer, 0, buffer.length);
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage());
            System.out.println(ex);
        }
        LOGGER.info("The file has been written");
        req.setAttribute("done", s);
        req.getRequestDispatcher("jsp/reportEventUser.jsp").forward(req, resp);
    }
}
