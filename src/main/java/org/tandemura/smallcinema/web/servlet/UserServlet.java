package org.tandemura.smallcinema.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.dao.exception.DBException;
import org.tandemura.smallcinema.dao.exception.DuplicateEntityException;
import org.tandemura.smallcinema.model.User;
import org.tandemura.smallcinema.service.UserService;
import org.tandemura.smallcinema.service.impl.UserServiceImpl;
import org.tandemura.smallcinema.service.exception.NotFoundEntityException;
import org.tandemura.smallcinema.service.util.Paginator;
import org.tandemura.smallcinema.service.util.Security;
import org.tandemura.smallcinema.service.util.UserCache;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The {@code UserServlet} class provides one of main functional for the Admins (CRUD).
 *      - View the list of users.
 *      - Create new user.
 *      - Edit created user data.
 *      - Delete the users from database.
 * Redirects to error page if some exception comes from DAO or service layer.
 */
public class   UserServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(UserServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        UserService service = UserServiceImpl.getUserService();
        String action = req.getParameter("action");

        try {
            switch (action == null ? "all" : action) {
                case "edit":
                case "create":
                    final User user = "create".equals(action) ?
                            new User() : service.get(Integer.parseInt(req.getParameter("userid")));
                    req.setAttribute("user", user);
                    req.getRequestDispatcher("jsp/userForm.jsp").forward(req, resp);
                    break;
                case "delete":
                    service.delete(Integer.parseInt(req.getParameter("userid")));
                    resp.sendRedirect("users" + getAdd(req));
                    break;
                case "all":
                    Paginator<User> paginator = new Paginator<>();
                    List<User> users = UserCache.getUsers();
                    int page = req.getParameter("page")!=null ? Integer.parseInt(req.getParameter("page")) : 1;
                    int pagesCount = paginator.getPageCount(users);
                    page = page > pagesCount ? pagesCount : page < 1 ? 1 : page;
                    req.setAttribute("users", paginator.getPage(users, page));
                    req.setAttribute("page", page);
                    req.setAttribute("pagesCount", paginator.getPageCount(users));
                    req.getRequestDispatcher("jsp/users.jsp").forward(req, resp);
                    break;
                default:
                    req.getRequestDispatcher("jsp/users.jsp").forward(req, resp);
                    break;
            }
        } catch (NumberFormatException | NotFoundEntityException e) {
            LOGGER.warn(e.getMessage());
            resp.setStatus(404);
            req.getRequestDispatcher("jsp/error404.jsp").forward(req, resp);
        } catch (DBException e) {
            LOGGER.error(e);
            resp.setStatus(500);
            req.getRequestDispatcher("jsp/error500.jsp").forward(req, resp);
        }
    }

    private String getAdd(HttpServletRequest req) {
        String referer = req.getHeader("referer");
        return referer.lastIndexOf("?") != -1 ? referer.substring(referer.lastIndexOf("?")) : "";
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String regName = req.getParameter("name");
        String regEmail = req.getParameter("email");
        String regPassword = Security.encrypt(req.getParameter("password"));
        String regRole = req.getParameter("role")!=null ? req.getParameter("role") : "CLIENT";
        UserService service = UserServiceImpl.getUserService();

        try {
            User user = new User(regName, regEmail, regPassword, User.Role.valueOf(regRole));
            if (req.getParameter("userid").isEmpty()) {
                service.create(user);
            }
            else {
                user.setId(Integer.parseInt(req.getParameter("userid")));
                service.update(user);
            }
            resp.sendRedirect("users");
        } catch (IllegalArgumentException e) {
            LOGGER.warn("Parsing error", e.getMessage());
            resp.setStatus(404);
            req.getRequestDispatcher("jsp/error404.jsp").forward(req, resp);
        } catch (DuplicateEntityException de) {
            LOGGER.warn(de);
            req.setAttribute("duplicate", true);
            User user = new User(regName, regEmail, null, null);
            if(req.getParameter("userid")!=null) {
                user.setId(Integer.parseInt(req.getParameter("userid")));
            }
            req.setAttribute("user", user);
            req.getRequestDispatcher("jsp/userForm.jsp").forward(req, resp);
        } catch (DBException e) {
            LOGGER.warn(e);
            resp.setStatus(500);
            req.getRequestDispatcher("jsp/error500.jsp").forward(req, resp);
        }
    }
}
