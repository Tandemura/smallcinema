package org.tandemura.smallcinema.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.dao.exception.DBException;
import org.tandemura.smallcinema.model.User;
import org.tandemura.smallcinema.service.UserService;
import org.tandemura.smallcinema.service.impl.UserServiceImpl;
import org.tandemura.smallcinema.service.exception.NotFoundEntityException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.tandemura.smallcinema.service.util.Security.encrypt;

/**
 * The {@code LoginServlet} class purpose is to validate user's credentials.
 * Invalidates the session and creates new one. After that stores user data into session.
 * Redirects to error page if some exception comes from DAO layer.
 */
public class LoginServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(LoginServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String authEmail = req.getParameter("authUserEmail");

        try{
            UserService userService = UserServiceImpl.getUserService();
            User aUser = userService.getByEmail(authEmail);
            if(aUser!=null && aUser.getPassword().equals(encrypt(req.getParameter("authUserPass")))) {
                HttpSession session = req.getSession();
                session.invalidate();
                session = req.getSession();
                session.setAttribute("authUser", aUser);
                LOGGER.info("{} logged in", aUser.getName());
                resp.sendRedirect("schedule");
            } else {
                throw new NotFoundEntityException("Wrong password");
            }
        } catch (NotFoundEntityException e) {
            LOGGER.warn(e.getMessage());
            req.setAttribute("error", "password");
            req.getRequestDispatcher("jsp/login.jsp").forward(req, resp);
        } catch (DBException e) {
            LOGGER.error(e);
            resp.setStatus(500);
            req.getRequestDispatcher("jsp/errorpages/error500.jsp").forward(req, resp);
        }
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/login.jsp").forward(req, resp);
    }
}

