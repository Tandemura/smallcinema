package org.tandemura.smallcinema.web.servlet;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.service.EventService;
import org.tandemura.smallcinema.service.TicketService;
import org.tandemura.smallcinema.service.impl.EventServiceImpl;
import org.tandemura.smallcinema.service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code ScheduleServlet} class purpose is to form a list of {@code Event} class objects
 * representing a daily schedule of planned Movies to be shown in the cinema.
 * Possible URL attributes:
 * date=somedate - somedate will be parsed to LocalDate and the records for specified
 * data will be returned.
 * Redirects to error page if some exception comes from DAO layer.
 */
public class ScheduleServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(ScheduleServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req,resp);
    }

    private List<LocalDate> getNextWeek() {
        List<LocalDate> nextWeek = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            nextWeek.add(LocalDate.now().plusDays(i));
        }
        return nextWeek;
    }
    private void doProcess(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        req.setCharacterEncoding("UTF-8");
        EventService service = EventServiceImpl.getEventService();
        TicketService ticketService = TicketServiceImpl.getTicketService();
        String date = req.getParameter("date");
        LocalDate localDate = null;
        try {
            localDate = date == null ? LocalDate.now() : LocalDate.parse(date);
            localDate = localDate.isBefore(LocalDate.now()) ? LocalDate.now() : localDate;
        } catch (DateTimeParseException e) {
            LOGGER.warn("Incorrect date input");
            resp.setStatus(404);
            req.getRequestDispatcher("jsp/error404.jsp").forward(req, resp);
        }
        req.setAttribute("events", service.getEvents(localDate));
        req.setAttribute("nextWeek", getNextWeek());
        req.setAttribute("date", localDate);

        req.getRequestDispatcher("jsp/schedule.jsp").forward(req, resp);
    }
}
