package org.tandemura.smallcinema.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.dao.exception.DBException;
import org.tandemura.smallcinema.model.Movie;
import org.tandemura.smallcinema.service.util.MovieCache;
import org.tandemura.smallcinema.service.util.Paginator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class HomepageServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(MovieManagerServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        String fl = String.valueOf(session.getAttribute("onlyActiveMovies"));
        if (fl == null) {
            session.setAttribute("onlyActiveMovies", "OFF");
        }
        String flag = session.getAttribute("onlyActiveMovies") == null ? "OFF" : (String) session.getAttribute("onlyActiveMovies");
        try {


            List<Movie> movies;
            if (flag.equals("ON")) {
                movies = MovieCache.getActiveMovies();
            } else {
                movies = MovieCache.getMovies();
            }
            doPaginate(req, resp, movies);
            req.getRequestDispatcher("jsp/home.jsp").forward(req, resp);
        } catch (DBException e) {
            LOGGER.error(e);
            resp.setStatus(500);
            req.getRequestDispatcher("jsp/errorpages/error500.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String flag;
        HttpSession session = req.getSession();
        String fl = String.valueOf(session.getAttribute("onlyActiveMovies"));
        if (fl != null) {
            if (fl.equals("ON")) {
                flag = "OFF";
                session.setAttribute("onlyActiveMovies", flag);
            } else {
                flag = "ON";
                session.setAttribute("onlyActiveMovies", flag);
            }
        } else {
            flag = "OFF";
            session.setAttribute("onlyActiveMovies", flag);
        }
        try {
            List<Movie> movies;
            if (flag.equals("ON")) {
                movies = MovieCache.getActiveMovies();
            } else {
                movies = MovieCache.getMovies();
            }
            doPaginate(req, resp, movies);
            req.getRequestDispatcher("jsp/home.jsp").forward(req, resp);
        } catch (DBException e) {
            LOGGER.error(e);
            resp.setStatus(500);
            req.getRequestDispatcher("jsp/errorpages/error500.jsp").forward(req, resp);
        }
    }

    private void doPaginate(HttpServletRequest req, HttpServletResponse resp, List<Movie> movies) {
        Paginator<Movie> paginator = new Paginator<>();
        String sPage = req.getParameter("page");
        int page = sPage != null ? Integer.parseInt(sPage) : 1;
        int pagesCount = paginator.getPageCount(movies);
        page = page > pagesCount ? pagesCount : Math.max(page, 1);
        req.setAttribute("movies", paginator.getPage(movies, page));
        req.setAttribute("page", page);
        req.setAttribute("pagesCount", paginator.getPageCount(movies));
    }
}