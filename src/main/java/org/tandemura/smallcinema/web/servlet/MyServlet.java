package org.tandemura.smallcinema.web.servlet;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.dao.exception.DBException;
import org.tandemura.smallcinema.model.User;
import org.tandemura.smallcinema.service.TicketService;
import org.tandemura.smallcinema.service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Shows list of Tickets owned by specific User.
 * Available only for authorized Users.
 */
public class MyServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(MyServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        User aUser = (User)session.getAttribute("authUser");
        TicketService service = TicketServiceImpl.getTicketService();

        if(aUser!=null) {
            try {
                req.setAttribute("tickets", service.getAllTicketsByUserId(aUser.getId()));
            } catch (DBException e) {
                LOGGER.error(e);
                resp.setStatus(500);
                req.getRequestDispatcher("jsp/errorpages/error500.jsp").forward(req, resp);
            }
            req.getRequestDispatcher("jsp/myTickets.jsp").forward(req, resp);
        } else {
            resp.setStatus(403);
            req.getRequestDispatcher("jsp/errorpages/error403.jsp").forward(req, resp);
        }
    }
}