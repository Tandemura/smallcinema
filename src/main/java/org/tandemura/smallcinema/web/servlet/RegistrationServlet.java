package org.tandemura.smallcinema.web.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.dao.exception.DBException;
import org.tandemura.smallcinema.dao.exception.DuplicateEntityException;
import org.tandemura.smallcinema.model.User;
import org.tandemura.smallcinema.service.UserService;
import org.tandemura.smallcinema.service.impl.UserServiceImpl;
import org.tandemura.smallcinema.web.util.UserDataValidation;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.tandemura.smallcinema.service.util.Security.encrypt;

/**
 * The {@code RegistrationServlet} class purpose is to allow user to register a new account.
 * This class perform validation of entered data before storing to database.
 * Invalidates the session and creates new one. After that stores user data into session.
 * Redirects to error page if some exception comes from DAO layer.
 */
public class RegistrationServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(RegistrationServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("jsp/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String regName = req.getParameter("regName");
        String regEmail = req.getParameter("regEmail");
        String regPassword = req.getParameter("regPass");
        String confPassword = req.getParameter("confPass");

        if (UserDataValidation.hasError(req, regName, regEmail, regPassword, confPassword)) {
            req.getRequestDispatcher("jsp/registration.jsp").forward(req, resp);
        } else {
            try {
                regPassword=encrypt(regPassword);
                UserService userService = UserServiceImpl.getUserService();
                User user = new User(regName, regEmail, regPassword, User.Role.CLIENT);
                userService.create(user);
                HttpSession session = req.getSession();
                session.invalidate();
                session = req.getSession();
                session.setAttribute("authUser", userService.getByEmail(regEmail));
                LOGGER.info("{} logged in", user.getName());
                resp.sendRedirect("schedule");
            } catch (DuplicateEntityException de) {
                LOGGER.warn(de.getMessage());
                req.setAttribute("duplicate", true);
                req.getRequestDispatcher("jsp/registration.jsp").forward(req, resp);
            } catch (DBException e) {
                resp.setStatus(500);
                req.getRequestDispatcher("jsp/errorpages/error500.jsp").forward(req, resp);
            }
        }
    }
}

