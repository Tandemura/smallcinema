package org.tandemura.smallcinema.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;

/**
 * Represents a row in the &quot;movies&quot; database table,
 * with each column mapped to a property of this class.
 */
public class Movie extends Entity {
    private String name;
    private String genre;
    private Integer duration;
    private Integer year;
    private Boolean active;
    private String poster;
    private String description;

    public Movie() {
    }

    public Movie(Integer id) {
        super(id);
    }

    public Movie(Integer id, String name, String genre, Integer duration, Integer year, Boolean active, String description, String poster) {
        super(id);
        this.name = name;
        this.genre = genre;
        this.duration = duration;
        this.year = year;
        this.active = active;
        this.poster = id == null ? "empty": id.toString();
        this.description = description;
    }

    public Movie(String name, String genre, Integer duration, Integer year, Boolean active, String description, String poster) {
        this(null, name, genre, duration, year, active, description, poster);
    }

    public Movie(ResultSet rs) throws SQLException {
        this(rs.getInt("m.mid"),
                rs.getString("m.name"),
                rs.getString("m.genre"),
                rs.getInt("m.duration"),
                rs.getInt("m.year"),
                rs.getBoolean("active"),
                rs.getString("description"),
                rs.getString("poster"));
    }

    public String getName() {
        return name;
    }

    public String getGenre() {
        return genre;
    }

    public Integer getDuration() {
        return duration;
    }

    public Integer getYear() {
        return year;
    }

    public String getPoster() {
            return poster;
    }

    public String getDescription() {
        return description;
    }

    public Boolean isActive() {
        return active;
    }


    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", year='" + year + '\'' +
                '}';
    }

    public static Comparator<Movie> NameComparator = new Comparator<Movie>() {
        @Override
        public int compare(Movie o1, Movie o2) {
            return o1.getName().compareTo(o2.getName());
        }
    };
}
