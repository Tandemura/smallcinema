package org.tandemura.smallcinema.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * The {@code Event} class represents an entity of cinema event.
 * Represents a row in the &quot;users&quot; database table,
 * with each column mapped to a property of this class.
 */
public class Event extends Entity {

    private LocalDate date;
    private Seance seance;
    private Movie movie;

    public Event(LocalDate date) {
        this(null, date, null, null);
    }

    public Event(Integer id, LocalDate date, Seance seance, Movie movie) {
        super(id);
        this.date = date;
        this.seance = seance;
        this.movie = movie;
    }

    public Event(ResultSet rs) throws SQLException {
        this(rs.getInt("e.eid"), rs.getDate("e.date").toLocalDate(),
                new Seance(rs), new Movie(rs));
    }

    public LocalDate getDate() {
        return date;
    }

    public Seance getSeance() {
        return seance;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public void setSeance(Seance seance) {
        this.seance = seance;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", date=" + date +
                ", seance=" + seance +
                '}';
    }
}
