package org.tandemura.smallcinema.model;

import java.io.Serializable;

/**
 * The {@code Entity} class represents an abstract entity.
 */
public abstract class Entity implements Serializable {
    protected Integer id;

    public Entity() {
    }

    public Entity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isNew() {
        return this.id == null;
    }

    public int hashCode() {
        return id == null ? 0 : id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Entity e = (Entity) o;

        return id.equals(e.id);
    }
}
