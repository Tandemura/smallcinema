package org.tandemura.smallcinema.dao.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.dao.exception.DBException;

import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;


public class ConnectionPool {
    static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);
    private ResourceBundle appRB;
    private final ResourceBundle dbRB;
    private static volatile ConnectionPool pool;
    private final Queue<DBConnection> available;
    private final Queue<DBConnection> active;
    private final int initialPoolSize;
    private final int maxPoolSize;

    public static ConnectionPool getPool() throws DBException {
        if (pool == null) {
            synchronized (ConnectionPool.class) {
                if (pool == null) {
                    pool = new ConnectionPool();
                }
            }
        }
        return pool;
    }

    private ConnectionPool() throws DBException {
        try {
            appRB = ResourceBundle.getBundle("application");
            dbRB = ResourceBundle.getBundle(appRB.getString("app.db"));
            initialPoolSize = Integer.parseInt(dbRB.getString("pool.initial"));
            maxPoolSize = Integer.parseInt(dbRB.getString("pool.max"));
            available = new ArrayBlockingQueue<>(maxPoolSize);
            active = new ArrayBlockingQueue<>(maxPoolSize);
            initConnectionPool();
            if(LOGGER.isDebugEnabled()) {
                LOGGER.debug("Connection pool created size of {}" + available.size());
            }
        } catch (MissingResourceException e) {
            LOGGER.error("DB settings file not found at {}", appRB.getString("app.db"));
            LOGGER.error(e);
            throw new DBException("DB settings not found");
        }
    }

    private void initConnectionPool() {
        for(int i = 0; i < initialPoolSize; i++) {
            available.add(createNewConnection());
        }
    }

    public synchronized DBConnection getConnection() {
        DBConnection con = null;
        while(con == null) {
            if(!available.isEmpty()) {
                con = available.poll();
                if(LOGGER.isDebugEnabled()) LOGGER.debug("Got connection from pool");
            }
            else if(available.isEmpty() && active.size() < maxPoolSize) {
                con = createNewConnection();
                if(LOGGER.isDebugEnabled()) LOGGER.debug("New extra connection created");
            }
        }
        active.add(con);
        return con;
    }

    private DBConnection createNewConnection() {
        DBConnection con = null;
        try {
            con = new DBConnection(dbRB.getString("database.driver"),
                    dbRB.getString("database.url"),
                    dbRB.getString("database.user"),
                    dbRB.getString("database.password"));
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error("Connection was not created", e);
            throw new DBException("Connection was not created");
        }
        return con;
    }

    void close(DBConnection con) {
        if(active.remove(con)) {
            available.add(con);
            if(LOGGER.isDebugEnabled()) LOGGER.debug("Connection returned to pool");
        }
    }
}

