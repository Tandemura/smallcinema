package org.tandemura.smallcinema.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.dao.BaseDao;
import org.tandemura.smallcinema.dao.JDBCDao;
import org.tandemura.smallcinema.model.Entity;
import org.tandemura.smallcinema.model.Movie;

import java.time.LocalDate;
import java.util.List;

/**
 * This class provides implementation of all {@code UserDao} interface methods.
 */
public class MovieDaoImpl implements BaseDao {
    private static final Logger LOGGER = LogManager.getLogger(MovieDaoImpl.class);

    private final JDBCDao simpleDao = new JDBCDaoImpl();

    private static volatile MovieDaoImpl movieDao;

    private MovieDaoImpl() {
    }

    public static MovieDaoImpl getMovieDao() {
        if (movieDao == null) {
            synchronized (MovieDaoImpl.class) {
                if (movieDao == null) {
                    movieDao = new MovieDaoImpl();
                }
            }
        }
        if(LOGGER.isDebugEnabled()) LOGGER.debug("MovieDaoImpl received");
        return movieDao;
    }

    @Override
    public List<Movie> getAll() {
        String query = "SELECT * FROM movies m";
        return simpleDao.getAllById(Movie.class, query);
    }

    @Override
    public Movie save(Entity entity) {
        Movie movie = (Movie) entity;
        if (movie.isNew()) {
            String query = "INSERT INTO movies (name, genre, duration, year, active, description, poster) VALUES (?, ?, ?, ?, ?, ?, ?)";
            return simpleDao.save(movie, query, movie.getName(), movie.getGenre(), movie.getDuration(), movie.getYear(),
                    movie.isActive(), movie.getDescription(), movie.getPoster());
        } else {
            String query = "UPDATE movies SET name=?, genre=?, duration=?, year=?, active=?, description=?, poster=? WHERE mid=?";
            return simpleDao.update(movie, query, movie.getName(), movie.getGenre(), movie.getDuration(), movie.getYear(),
                    movie.isActive(), movie.getDescription(), movie.getId(), movie.getId());
        }
    }

    @Override
    public boolean delete(int id) {
        String query = "DELETE FROM movies WHERE mid=?;";
        return simpleDao.delete(query, id);
    }

    @Override
    public Movie getById(int id) {
        String query = "SELECT * FROM movies m WHERE mid=?";
        return simpleDao.getById(Movie.class, query, id);
    }

    @Override
    public List<Movie> getAllByDate(LocalDate date) {
        return null;
    }

    @Override
    public Movie getByValue(String value) {
        return null;
    }
}

