package org.tandemura.smallcinema.dao.impl;

import org.tandemura.smallcinema.dao.BaseDao;

public class DaoFactory {

    public static BaseDao getDao(DaoType type) {
        switch (type) {
            case USER:
                return UserDaoImpl.getUserDao();
            case MOVIE:
                return MovieDaoImpl.getMovieDao();
            case EVENT:
                return EventDaoImpl.getEventDao();
            default:
                throw new EnumConstantNotPresentException(DaoType.class, type.name());
        }
    }

    public enum DaoType {
        EVENT,
        MOVIE,
        USER
    }
}