package org.tandemura.smallcinema.dao.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.dao.JDBCDao;
import org.tandemura.smallcinema.dao.exception.DBException;
import org.tandemura.smallcinema.dao.exception.DuplicateEntityException;
import org.tandemura.smallcinema.dao.util.ConnectionPool;
import org.tandemura.smallcinema.dao.util.DBConnection;
import org.tandemura.smallcinema.model.Entity;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides implementation of all {@code JDBCDao} interface methods.
 */
public class JDBCDaoImpl implements JDBCDao {
    private static final Logger LOGGER = LogManager.getLogger(JDBCDaoImpl.class);

    @Override
    public boolean delete(String query, int id) {
        int resultRows = 0;
        try (DBConnection con = ConnectionPool.getPool().getConnection()) {
            PreparedStatement pst = con.prepareStatement(query, id);
            resultRows = pst.executeUpdate();
            LOGGER.info("Deleted entity id={}", id);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new DBException("Unable to delete record");
        }
        return resultRows == 1;
    }

    @Override
    public <T extends Entity> T getById(Class cl, String query, Object... values) {
        T entity = null;
        try (DBConnection con = ConnectionPool.getPool().getConnection()) {
            PreparedStatement pst = con.prepareStatement(query, values);
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {
                entity = (T) cl.getConstructor(ResultSet.class).newInstance(rs);
                LOGGER.info("Entity obtained: " + entity.toString());
            }
            rs.close();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DBException("Unable to obtain record");
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            LOGGER.error(e);
        }
        return entity;
    }

    public <T extends Entity> List<T> getAllById(Class cl, String query, Object... values) {
        List<T> list = new ArrayList<>();
        T entity = null;
        try (DBConnection con = ConnectionPool.getPool().getConnection()) {
            PreparedStatement pst = con.prepareStatement(query, values);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                entity = (T) cl.getConstructor(ResultSet.class).newInstance(rs);
                list.add(entity);
                LOGGER.info("Entity obtained: " + entity.toString());
            }
            rs.close();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new DBException("Unable to obtain record");
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            LOGGER.error(e);
        }
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends Entity> T save(T entity, String query, Object... values) {
        try (DBConnection con = ConnectionPool.getPool().getConnection()) {
            PreparedStatement pst = con.prepareInsertStatement(query, values);
            if (pst.executeUpdate() == 1) {
                ResultSet rs = pst.getGeneratedKeys();
                rs.next();
                entity.setId(rs.getInt(1));
                LOGGER.info("New entity created with id={}", entity.getId());
                rs.close();
            }
        } catch (SQLException e) {
            if (e.getMessage().contains("Duplicate")) {
                LOGGER.error(e);
                throw new DuplicateEntityException();
            }
            LOGGER.error(e);
            throw new DBException("Unable to saveAll new record");
        }
        return entity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends Entity> T update(T entity, String query, Object... values) {
        try (DBConnection con = ConnectionPool.getPool().getConnection()) {
            PreparedStatement pst = con.prepareStatement(query, values);
            if (pst.executeUpdate() != 1) {
                LOGGER.error("Entity was not not updated");
                throw new DBException("Unable to update record");
            }
            LOGGER.info("User data successfully updated for id=" + entity.getId());
        } catch (SQLException e) {
            if (e.getMessage().contains("Duplicate")) {
                LOGGER.error(e);
                throw new DuplicateEntityException();
            }
            LOGGER.error(e);
            throw new DBException("Unable to saveAll new record");
        }
        return entity;
    }
}
