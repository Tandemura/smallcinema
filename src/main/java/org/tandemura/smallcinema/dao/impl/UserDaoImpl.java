package org.tandemura.smallcinema.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.tandemura.smallcinema.dao.BaseDao;
import org.tandemura.smallcinema.dao.JDBCDao;
import org.tandemura.smallcinema.model.Entity;
import org.tandemura.smallcinema.model.User;

import java.time.LocalDate;
import java.util.List;

/**
 * This class provides implementation of all {@code BaseDao} interface methods.
 */
public class UserDaoImpl implements BaseDao {
    private static final Logger LOGGER = LogManager.getLogger(UserDaoImpl.class);

    private final JDBCDao simpleDao = new JDBCDaoImpl();

    private static volatile UserDaoImpl userDao;

    private UserDaoImpl() {}

    public static UserDaoImpl getUserDao() {
        if(userDao==null) {
            synchronized (UserDaoImpl.class) {
                if(userDao==null) {
                    userDao = new UserDaoImpl();
                }
            }
        }
        if(LOGGER.isDebugEnabled()) LOGGER.debug("UserDao received");
        return userDao;
    }

    @Override
    public List<User> getAll() {
        String query = "SELECT * FROM users u";
        return simpleDao.getAllById(User.class, query);
    }

    @Override
    public User save(Entity entity) {
        User user = (User)entity;
        if (user.isNew()) {
            String query = "INSERT INTO users (name, email, password, role) VALUES (?, ?, ?, ?)";
            return simpleDao.save(user, query, user.getName(), user.getEmail(), user.getPassword(), User.Role.CLIENT.toString());
        } else {
            if(user.getPassword().isEmpty()) {
                String query = "UPDATE users SET name=?, email=?, role=? WHERE uid=?";
                return simpleDao.update(user, query, user.getName(), user.getEmail(), user.getRole().toString(), user.getId());
            } else {
                String query = "UPDATE users SET name=?, email=?, password=?, role=? WHERE uid=?";
                return simpleDao.update(user, query, user.getName(), user.getEmail(), user.getPassword(), user.getRole().toString(), user.getId());
            }
        }
    }

    @Override
    public boolean delete(int id) {
        String query = "DELETE FROM users WHERE uid=?;";
        return simpleDao.delete(query, id);
    }

    @Override
    public User getByValue(String email) {
        String query = "SELECT * FROM users u WHERE email=?";
        return getUser(query, email);
    }

    @Override
    public User getById(int id) {
        String query = "SELECT * FROM users u WHERE uid=?";
        return getUser(query, id);
    }

    private User getUser(String query, Object... values) {
        return simpleDao.getById(User.class, query, values);
    }

    @Override
    public <T extends Entity> List<T> getAllByDate(LocalDate date) {
        return null;
    }

}
