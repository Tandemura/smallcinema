<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="footer" %>
<%--Localization--%>
<fmt:setLocale value="${sessionScope.language!=null ? sessionScope.language : pageContext.request.locale}"/>
<fmt:setBundle basename="messages.app"/>
<%----%>
<html>
<jsp:include page="fragments/headTag.jsp"/>
<body>
<jsp:include page="fragments/navMenu.jsp"/>
<div class="films">
    <c:if test="${param.action == 'edit'}">
        <h2><fmt:message key="entity.editdata"/></h2>
    </c:if>
    <c:if test="${param.action == 'create'}">
        <h2><fmt:message key="entity.create"/></h2>
    </c:if>
    <hr>
    <jsp:useBean id="movie" type="org.tandemura.smallcinema.model.Movie" scope="request"/>
    <div id="movieForm">
        <form method="post" action="movies">
            <input type="hidden" name="mid" value="${movie.id}">

            <div><label><fmt:message key="entity.name"/></label></div>
            <div><input type="text" value="${movie.name}" name="name" required></div>
            <br>
            <div><label><fmt:message key="movie.genre"/></label></div>
            <div><input type="text" value="${movie.genre}" name="genre" required></div>
            <br>
            <div><label><fmt:message key="movie.duration"/></label></div>
            <div><input type="text" value="${movie.duration}" name="duration" required></div>
            <br>
            <div><label><fmt:message key="movie.year"/></label></div>
            <div><input type="text" value="${movie.year}" name="year" required></div>
            <br>
            <div><label><fmt:message key="movie.description"/></label></div>
            <div><input type="text" value="${movie.description}" name="description" width="347px" height="176px"></div>
            <br>
            <div><label><fmt:message key="movie.status"/></label></div>
            <div><select name="status">
                <option value="true"><fmt:message key="movie.active"/></option>
                <option value="false"><fmt:message key="movie.archived"/></option>
            </select></div>
            </br>
            <div><label><fmt:message key="movie.poster"/> : ${movie.poster}</label>.jpg</div>
            </br>
            <button type="submit"><fmt:message key="entity.save"/></button>
            <button onclick="window.history.back()" type="button"><fmt:message key="entity.cancel"/></button>
        </form>
    </div>
</div>
<footer:footer/>
</body>
</html>

