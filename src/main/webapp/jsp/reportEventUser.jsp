<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="footer" %>
<%--Localization--%>
<fmt:setLocale value="${sessionScope.language!=null ? sessionScope.language : pageContext.request.locale}"/>
<fmt:setBundle basename="messages.app"/>
<html>
<jsp:include page="fragments/headTag.jsp"/>
<head>
    <title>Title</title>
</head>
<body>
<jsp:include page="fragments/navMenu.jsp"/>
<div class="films">

    <div id="loginForm">
        <form method="post" action="report">
            <input type="hidden" name="rep" value="doReport">
            <button type="submit"><fmt:message key="app.report"/></button>
        </form>
    </div>
    <br>
    <div><c:out value="${done}"/></div>


</div>
<footer:footer/>
</body>
</html>
