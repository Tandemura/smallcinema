<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="footer"%>
<%--Localization--%>
<fmt:setLocale value="${sessionScope.language!=null ? sessionScope.language : pageContext.request.locale}"/>
<fmt:setBundle basename="messages.app"/>
<%----%>
<html>
<jsp:include page="fragments/headTag.jsp"/>
<body>
<jsp:include page="fragments/navMenu.jsp"/>
<div class="films">
    <br>
    <div><a href="movies?action=create"><fmt:message key="entity.add"/></a></div>
    <div id="shownPage">
        <div>
            <c:if test="${page > 1}"><a href="movies?page=${page > 1 ? page - 1 : 1}">< < <</a></c:if>
            <c:if test="${page <= 1}">< < <</c:if>
        </div>
        <div>Page&nbsp;${page}&nbsp;of&nbsp;${pagesCount}</div>
        <div>
            <c:if test="${page < pagesCount}"><a href="movies?page=${page < pagesCount ? page + 1 : pagesCount}">> >
                ></a></c:if>
            <c:if test="${page >= pagesCount}">> > ></c:if>
        </div>
    </div>
    <table>
        <thead>
        <tr>
            <th><fmt:message key="movie.poster"/></th>
            <th><fmt:message key="movie.info"/></th>
            <th><fmt:message key="movie.status"/></th>
            <th><fmt:message key="movie.description"/></th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <c:forEach items="${movies}" var="movie">
            <jsp:useBean id="movie" type="org.tandemura.smallcinema.model.Movie"/>
            <tr>
                <td><img src="images/<c:out value="${movie.poster}"/>.jpg"
                         width="200">
                </td>
                <td>
                    <p><c:out value="${movie.name}"/></p>
                    <hr>
                    <p><c:out value="${movie.genre}"/></p>
                    <p><c:out value="${movie.duration}"/> <fmt:message key="movie.minutes"/></p>
                    <p><c:out value="${movie.year}"/></p>
                </td>

                <td><c:if test="${movie.isActive()}">Active</c:if>
                    <c:if test="${!movie.isActive()}">Archived</c:if></td>
                <td><p align="justify">${movie.description}</p></td>
                <td><a href="movies?action=edit&mid=${movie.id}"><fmt:message key="entity.edit"/></a></td>
                <td><a href="movies?action=delete&mid=${movie.id}"><fmt:message key="entity.delete"/></a></td>
            </tr>
        </c:forEach>
    </table>
</div>
<footer:footer/>
</body>
</html>
