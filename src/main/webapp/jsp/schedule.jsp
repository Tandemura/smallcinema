<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="footer" %>
<%--Localization--%>
<fmt:setLocale value="${sessionScope.language!=null ? sessionScope.language : pageContext.request.locale}"/>
<fmt:setBundle basename="messages.app"/>
<%----%>
<html>
<jsp:include page="fragments/headTag.jsp"/>
<body>
<jsp:include page="fragments/navMenu.jsp"/>
<div class="week">
    <c:forEach items="${nextWeek}" var="day">
        <jsp:useBean id="day" type="java.time.LocalDate"/>
        <div><a href="schedule?date=${day}"><fmt:message key="sch.${day.dayOfWeek}"/></a></div>
    </c:forEach>
</div>

<br>
<div class="films">
    <c:if test="${sessionScope.authUser.role == 'ADMIN'}">
        <div><a href="event?action=create&date=${date}"><fmt:message key="entity.add"/></a></div>
    </c:if>
    <br>
    <div>
        <div id="chosenDay">
            <div><fmt:message key="sch.seances"/>${date}</div>
        </div>


        <table>
            <thead>
            <tr>
                <th><fmt:message key="movie.poster"/></th>
                <th><fmt:message key="entity.name"/></th>
                <th><fmt:message key="movie.start"/></th>
                <c:if test="${sessionScope.authUser.role == 'ADMIN'}">
                    <th></th>
                </c:if>
                <th><fmt:message key="app.action"/></th>

            </tr>
            </thead>
            <c:forEach items="${events}" var="event">
                <jsp:useBean id="event" type="org.tandemura.smallcinema.model.Event"/>

                <tr>
                    <td><a href="event?action=view&eid=${event.id}"></a>
                        <div>
                            <img src="images/<c:out value="${event.movie.poster}"/>.jpg"
                                 width="250"></a>
                        </div>
                    </td>
                    <td><label><c:out value="${event.movie.name}"/></label>
                        <hr>
                        <div><p align="justify"><c:out value="${event.movie.description}"/></p></div>
                    </td>
                    <td>${event.seance.start}</td>
                    <c:if test="${sessionScope.authUser.role != 'ADMIN'}">
                        <td><a href="event?action=view&eid=${event.id}"><fmt:message key="action.buy"/></a></td>
                    </c:if>
                    <c:if test="${sessionScope.authUser.role == 'ADMIN'}">
                        <td><a href="event?action=view&eid=${event.id}"><fmt:message key="action.hallViev"/></a></td>
                        <td><a href="event?action=delete&eid=${event.id}"><fmt:message key="entity.delete"/></a></td>
                    </c:if>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
<footer:footer/>
</body>
</html>
