<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="footer" %>
<%--Localization--%>
<fmt:setLocale value="${sessionScope.language!=null ? sessionScope.language : pageContext.request.locale}"/>
<fmt:setBundle basename="messages.app"/>
<%----%>
<html>
<jsp:include page="fragments/headTag.jsp"/>
<body>
<jsp:include page="fragments/navMenu.jsp"/>
<div class="films">
    <br>
    <jsp:useBean id="event" scope="request" type="org.tandemura.smallcinema.model.Event"/>
    <fmt:message key="movie.name"/>: ${event.movie.name}<br>
    <fmt:message key="movie.genre"/>: ${event.movie.genre}<br>
    <fmt:message key="movie.duration"/>: ${event.movie.duration} <fmt:message key="movie.minutes"/><br>
    <fmt:message key="movie.year"/>: ${event.movie.year}<br>
    <fmt:message key="entity.date"/>: ${event.date}<br>
    <fmt:message key="movie.start"/>: ${event.seance.start}
    <br>
    <br>
    <fmt:message key="hall.select.seats"/>
    <table>
        <jsp:useBean id="hall" scope="request" type="org.tandemura.smallcinema.model.Hall"/>
        <c:forEach items="${hall.rows}" var="row">
            <tr>
                <c:forEach items="${row.seats}" var="seat">
                    <c:if test="${sessionScope.authUser==null}">
                        <c:if test="${seat.userId==null}">
                            <td class="black">${seat.row}-${seat.seat}</td>
                        </c:if>
                        <c:if test="${seat.userId!=null}">
                            <td class="grey">${seat.row}-${seat.seat}</td>
                        </c:if>
                    </c:if>

                    <c:if test="${sessionScope.authUser!=null}">
                        <c:if test="${seat.userId==null}">
                            <td><a class="black"
                                   href="ticket?eid=${event.id}&tid=${seat.row}-${seat.seat}">${seat.row}-${seat.seat}</a>
                            </td>
                        </c:if>
                        <c:if test="${seat.userId==sessionScope.authUser.id}">
                            <c:if test="${seat.id==null}">
                                <td><a class="blue"
                                       href="ticket?eid=${event.id}&tid=${seat.row}-${seat.seat}">${seat.row}-${seat.seat}</a>
                                </td>
                            </c:if>
                            <c:if test="${seat.id!=null}">
                                <td class="blue">${seat.row}-${seat.seat}</td>
                            </c:if>
                        </c:if>
                        <c:if test="${seat.userId!=null && seat.userId!=sessionScope.authUser.id}">
                            <td class="grey">${seat.row}-${seat.seat}</td>
                        </c:if>
                    </c:if>
                </c:forEach>
            </tr>
        </c:forEach>
    </table>
    <form method="post" action="ticket">
        <input type="hidden" name="eid" value="${event.id}">
        <c:forEach items="${sessionScope.tickets}" var="ticket">
            <jsp:useBean id="ticket" type="org.tandemura.smallcinema.model.Ticket"/>
            <c:out value="eid:${ticket.eventId}, row:${ticket.row}, seat:${ticket.seat}"/> <br>
        </c:forEach>
        <c:if test="${sessionScope.authUser != null}">
            <c:if test="${sessionScope.authUser.role != 'ADMIN'}">
                <button type="submit">
                    <fmt:message key="action.buy"/>
                </button>
            </c:if>
        </c:if>
        <c:if test="${sessionScope.authUser == null}">
            <div><h1><fmt:message key="app.notlogged"/></h1></div>
        </c:if>
    </form>
</div>
<footer:footer/>
</body>
</html>
