<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="m" uri="/WEB-INF/mytag.tld" %>
<%--Localization--%>
<fmt:setLocale value="${sessionScope.language!=null ? sessionScope.language : pageContext.request.locale}"/>
<fmt:setBundle basename="messages.app"/>
<%----%>
<html>
<head>
    <jsp:include page="fragments/headTag.jsp"/>
</head>
<body>
<jsp:include page="fragments/navMenu.jsp"/>
<div class="films">
    <div><span>
        Current Date and Time is: <m:today/>
    </span>
    </div>
    <br>
    <div class="check-box">
        <form method="post" action="home">
            <p>
                <c:if test="${onlyActiveMovies eq 'ON'}"><fmt:message key="show.active"/></c:if>
                <c:if test="${onlyActiveMovies ne 'ON'}"><fmt:message key="show.all"/></c:if>
            </p>
            <p>
                <input type="submit" name="button" value="<fmt:message key="action.change"/>"/>
            </p>
        </form>
    </div>
    <br>
    <div id="shownPage">
        <div>
            <c:if test="${page > 1}"><a href="home?page=${page > 1 ? page - 1 : 1}">< < <</a></c:if>
            <c:if test="${page <= 1}">< < <</c:if>
        </div>
        <div>Page&nbsp;${page}&nbsp;of&nbsp;${pagesCount}</div>
        <div>
            <c:if test="${page < pagesCount}"><a href="home?page=${page < pagesCount ? page + 1 : pagesCount}">> >
                ></a></c:if>
            <c:if test="${page >= pagesCount}">> > ></c:if>
        </div>
    </div>
    <c:forEach items="${movies}" var="movie">
            <div class="entry">
                <jsp:useBean id="movie" type="org.tandemura.smallcinema.model.Movie"/>
                <div class="entry-title h2">
                    <c:out value="${movie.name}"/>
                </div>
                <div class="entry-active">
                    <c:if test="${movie.isActive()}"><fmt:message key="movie.active"/></c:if>
                    <c:if test="${!movie.isActive()}"><fmt:message key="movie.archived"/></c:if>
                </div>
                <hr class="entry_header-divider">
                <div class="entry_content">
                    <div>
                        <img src="images/<c:out value="${movie.poster}"/>.jpg" width="230">
                    </div>
                    <div class="entry_content-description">
                        <div>
                            <c:out value="${movie.description}"/>
                        </div>
                        <div class="entry_info">
                            <fmt:message key="movie.genre"/>: <c:out value="${movie.genre}"/>,
                            <fmt:message key="movie.year"/>: <c:out value="${movie.year}"/>,
                            <fmt:message key="movie.duration"/>: <c:out value="${movie.duration}"/>
                        </div>
                    </div>
                </div>
            </div>
    </c:forEach>
</div>
<footer:footer/>
</body>
</html>
