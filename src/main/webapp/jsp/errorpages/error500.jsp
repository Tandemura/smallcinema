<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--Localization--%>
<fmt:setLocale value="${sessionScope.language!=null ? sessionScope.language : pageContext.request.locale}"/>
<c:if test="${sessionScope.locale == null}">
    <fmt:setLocale value="en"/>
</c:if>
<fmt:setBundle basename="messages.app"/>
<%----%>
<html>
<jsp:include page="../fragments/headTag.jsp"/>
<head>
    <title>Error 500</title>
</head>
<body class="error-page">
<div>
    <h1>Error 500</h1>
</div>
</body>
</html>
