<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Footer" pageEncoding="UTF-8"%>
<%@attribute name="footer" fragment="true" %>
<fmt:setLocale value="${sessionScope.language!=null ? sessionScope.language : pageContext.request.locale}" />
<fmt:setBundle basename="messages.app"/>
<footer>
    <div class="footer">
        Copyright(C)
    </div>
</footer>