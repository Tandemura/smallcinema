DROP TABLE IF EXISTS tickets;
DROP TABLE IF EXISTS events;
DROP TABLE IF EXISTS seances;
DROP TABLE IF EXISTS movies;
DROP TABLE IF EXISTS users;

-- -----------------------------------------------------
-- Table `movies`
-- -----------------------------------------------------
CREATE TABLE movies
(
    `mid`      INT          NOT NULL AUTO_INCREMENT,
    `name`     VARCHAR(255) NOT NULL,
    `genre`    VARCHAR(255) NOT NULL,
    `duration` INT          NOT NULL,
    `year`     INT          NOT NULL,
    `active`   TINYINT(1)   NOT NULL,
    `poster`   VARCHAR(45)  NOT NULL DEFAULT 'empty',
	`description` VARCHAR(1000) NULL DEFAULT 'There is not description yet.',
    PRIMARY KEY (`mid`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `seances`
-- -----------------------------------------------------
CREATE TABLE seances
(
    `sid`  INT  NOT NULL AUTO_INCREMENT,
    `time` TIME NOT NULL,
    PRIMARY KEY (`sid`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `events`
-- -----------------------------------------------------
CREATE TABLE events
(
    `eid`       INT  NOT NULL AUTO_INCREMENT,
    `movie_id`  INT  NOT NULL,
    `date`      DATE NULL DEFAULT NULL,
    `seance_id` INT  NULL DEFAULT NULL,
    PRIMARY KEY (`eid`),
    UNIQUE INDEX `seance_unique` (`seance_id` ASC, `date` ASC) VISIBLE,
    INDEX `movie_id` (`movie_id` ASC) VISIBLE,
    CONSTRAINT `events_ibfk_1`
        FOREIGN KEY (`movie_id`)
            REFERENCES `movies` (`mid`)
            ON DELETE CASCADE,
    CONSTRAINT `events_ibfk_2`
        FOREIGN KEY (`seance_id`)
            REFERENCES `seances` (`sid`)
            ON DELETE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
CREATE TABLE users
(
    `uid`      INT                      NOT NULL AUTO_INCREMENT,
    `name`     VARCHAR(255)             NOT NULL,
    `email`    VARCHAR(255)             NOT NULL,
    `password` VARCHAR(255)             NOT NULL,
    `role`     ENUM ('CLIENT', 'ADMIN') NULL DEFAULT NULL,
    PRIMARY KEY (`uid`),
    UNIQUE INDEX `email` (`email` ASC) VISIBLE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 2
    DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tickets`
-- -----------------------------------------------------
CREATE TABLE tickets
(
    `tid`      INT NOT NULL AUTO_INCREMENT,
    `event_id` INT NOT NULL,
    `user_id`  INT NULL DEFAULT NULL,
    `a_row`    INT NULL DEFAULT NULL,
    `seat`     INT NULL DEFAULT NULL,
    PRIMARY KEY (`tid`),
    UNIQUE INDEX `seat_unique` (`event_id` ASC, `a_row` ASC, `seat` ASC) INVISIBLE,
    INDEX `user_id` (`user_id` ASC) VISIBLE,
    CONSTRAINT `tickets_ibfk_1`
        FOREIGN KEY (`event_id`)
            REFERENCES `events` (`eid`)
            ON DELETE CASCADE,
    CONSTRAINT `tickets_ibfk_2`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`uid`)
            ON DELETE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
